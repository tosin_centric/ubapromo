import APP_CONFIG from './app.config';
import axios from 'axios';
import store from 'store';
class HttpService {
    constructor() {
        this.config = new APP_CONFIG();
        axios.interceptors.request.use(function (config) {
            let key = store.get('AUTH_KEY');
            if (key) {
                config.headers = {
                    "Content-Type": "application/json",
                    Authorization = `Bearer ${key}`
                }
                return config;
            } else {
                store.clearAll();
                return
            }
        }, function (err) {
            console.log(err)
            return Promise.reject(err);
        });
    }
    async Post(path, data) {
        axios.post(this.config.BASE_URL + path, data).then(res => {
            return res;
        }).catch(error => {
            return error;
        })
    }
    async FileUpload(path, data) {
        axios.post(this.config.BASE_URL + path, data).then(res => {
            return res;
        }).catch(error => {
            return error;
        })
    }

}
export default HttpService;