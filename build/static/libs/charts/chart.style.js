// "use strict";
// !function() {
//     var e = {
//             300: "#E3EBF6",
//             600: "#95AAC9",
//             700: "#6E84A3",
//             800: "#152E4D",
//             900: "#283E59"
//         },
//         t = {
//             100: "#D2DDEC",
//             300: "#A6C5F7",
//             700: "#2C7BE5"
//         },
//         a = "#FFFFFF",
//         o = "transparent",
//         l = "inhetit",
//         r = document.querySelectorAll('[data-toggle="chart"]'),
//         n = document.querySelectorAll('[data-toggle="legend"]');

//     function f(t) {
//         var a = void 0;
//         return Chart.helpers.each(Chart.instances, function(e) {
//             t == e.chart.canvas && (a = e)
//         }), a
//     }
//     "undefined" != typeof Chart && (Chart.defaults.global.responsive = !0, Chart.defaults.global.maintainAspectRatio = !1, Chart.defaults.global.defaultColor = e[600], Chart.defaults.global.defaultFontColor = e[600], Chart.defaults.global.defaultFontFamily = l, Chart.defaults.global.defaultFontSize = 13, Chart.defaults.global.layout.padding = 0, Chart.defaults.global.legend.display = !1, Chart.defaults.global.legend.position = "bottom", Chart.defaults.global.legend.labels.usePointStyle = !0, Chart.defaults.global.legend.labels.padding = 16, Chart.defaults.global.elements.point.radius = 0, Chart.defaults.global.elements.point.backgroundColor = t[700], Chart.defaults.global.elements.line.tension = .4, Chart.defaults.global.elements.line.borderWidth = 3, Chart.defaults.global.elements.line.borderColor = t[700], Chart.defaults.global.elements.line.backgroundColor = o, Chart.defaults.global.elements.line.borderCapStyle = "rounded", Chart.defaults.global.elements.rectangle.backgroundColor = t[700], Chart.defaults.global.elements.rectangle.maxBarThickness = 10, Chart.defaults.global.elements.arc.backgroundColor = t[700], Chart.defaults.global.elements.arc.borderColor = a, Chart.defaults.global.elements.arc.borderWidth = 4, Chart.defaults.global.elements.arc.hoverBorderColor = a, Chart.defaults.global.tooltips.enabled = !1, Chart.defaults.global.tooltips.mode = "index", Chart.defaults.global.tooltips.intersect = !1, Chart.defaults.global.tooltips.custom = function(r) {
//         var e = document.getElementById("chart-tooltip"),
//             n = this._chart.config.type;
//         if (e || ((e = document.createElement("div")).setAttribute("id", "chart-tooltip"), e.setAttribute("role", "tooltip"), e.classList.add("popover"), e.classList.add("bs-popover-top"), document.body.appendChild(e)), 0 !== r.opacity) {
//             if (r.body) {
//                 var t = r.title || [],
//                     a = r.body.map(function(e) {
//                         return e.lines
//                     }),
//                     s = '<div class="arrow"></div>';
//                 t.forEach(function(e) {
//                     s += '<h3 class="popover-header text-center">' + e + "</h3>"
//                 }), a.forEach(function(e, t) {
//                     var a = r.labelColors[t],
//                         o = '<span class="popover-body-indicator" style="background-color: ' + ("line" === n && "rgba(0,0,0,0.1)" !== a.borderColor ? a.borderColor : a.backgroundColor) + '"></span>',
//                         l = 1 < e.length ? "justify-content-left" : "justify-content-center";
//                     s += '<div class="popover-body d-flex align-items-center ' + l + '">' + o + e + "</div>"
//                 }), e.innerHTML = s
//             }
//             var o = this._chart.canvas.getBoundingClientRect(),
//                 l = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,
//                 i = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0,
//                 c = o.top + l,
//                 d = o.left + i,
//                 u = e.offsetWidth,
//                 f = e.offsetHeight,
//                 h = c + r.caretY - f - 16,
//                 p = d + r.caretX - u / 2;
//             e.style.top = h + "px", e.style.left = p + "px", e.style.visibility = "visible"
//         } else e.style.visibility = "hidden"
//     }, Chart.defaults.global.tooltips.callbacks.label = function(e, t) {
//         var a = "",
//             o = e.yLabel,
//             l = t.datasets[e.datasetIndex],
//             r = l.label,
//             n = l.yAxisID ? l.yAxisID : 0,
//             s = this._chart.options.scales.yAxes,
//             i = s[0];
//         if (n) i = s.filter(function(e) {
//             return e.id == n
//         })[0];
//         var c = i.ticks.callback;
//         return 1 < t.datasets.filter(function(e) {
//             return !e.hidden
//         }).length && (a = '<span class="popover-body-label mr-auto">' + r + "</span>"), a += '<span class="popover-body-value">' + c(o) + "</span>"
//     }, Chart.defaults.doughnut.cutoutPercentage = 83, Chart.defaults.doughnut.tooltips.callbacks.title = function(e, t) {
//         return t.labels[e[0].index]
//     }, Chart.defaults.doughnut.tooltips.callbacks.label = function(e, t) {
//         var a = t.datasets[0].data[e.index],
//             o = this._chart.options.tooltips.callbacks,
//             l = o.afterLabel() ? o.afterLabel() : "";
//         return '<span class="popover-body-value">' + (o.beforeLabel() ? o.beforeLabel() : "") + a + l + "</span>"
//     }, Chart.defaults.doughnut.legendCallback = function(e) {
//         var o = e.data,
//             l = "";
//         return o.labels.forEach(function(e, t) {
//             var a = o.datasets[0].backgroundColor[t];
//             l += '<span class="chart-legend-item">', l += '<i class="chart-legend-indicator" style="background-color: ' + a + '"></i>', l += e, l += "</span>"
//         }), l
//     }, Chart.scaleService.updateScaleDefaults("linear", {
//         gridLines: {
//             borderDash: [2],
//             borderDashOffset: [2],
//             color: e[300],
//             drawBorder: !1,
//             drawTicks: !1,
//             zeroLineColor: e[300],
//             zeroLineBorderDash: [2],
//             zeroLineBorderDashOffset: [2]
//         },
//         ticks: {
//             beginAtZero: !0,
//             padding: 10,
//             stepSize: 10
//         }
//     }), Chart.scaleService.updateScaleDefaults("category", {
//         gridLines: {
//             drawBorder: !1,
//             drawOnChartArea: !1,
//             drawTicks: !1
//         },
//         ticks: {
//             padding: 20
//         }
//     }), r && [].forEach.call(r, function(e) {
//         var t = e.dataset.trigger;
//         e.addEventListener(t, function() {
//             ! function(e) {
//                 var t = e.dataset.target,
//                     a = e.dataset.action,
//                     o = parseInt(e.dataset.dataset),
//                     l = f(document.querySelector(t));
//                 if ("toggle" === a) {
//                     var r = l.data.datasets,
//                         n = r.filter(function(e) {
//                             return !e.hidden
//                         })[0],
//                         s = r.filter(function(e) {
//                             return 1e3 === e.order
//                         })[0];
//                     if (!s) {
//                         for (var i in s = {}, n) "_meta" !== i && (s[i] = n[i]);
//                         s.order = 1e3, s.hidden = !0, r.push(s)
//                     }
//                     var c = r[o] === n ? s : r[o];
//                     for (var i in n) "_meta" !== i && (n[i] = c[i]);
//                     l.update()
//                 }
//                 if ("add" === a) {
//                     var d = l.data.datasets[o],
//                         u = d.hidden;
//                     d.hidden = !u
//                 }
//                 l.update()
//             }(e)
//         })
//     }), n && document.addEventListener("DOMContentLoaded", function() {
//         [].forEach.call(n, function(e) {
//             var t, a, o;
//             a = f(t = e).generateLegend(), o = t.dataset.target, document.querySelector(o).innerHTML = a
//         })
//     }))
// }(),
// function() {
//     var e = {
//             300: "#E3EBF6",
//             600: "#95AAC9",
//             700: "#6E84A3",
//             800: "#152E4D",
//             900: "#283E59"
//         },
//         t = localStorage.getItem("dashkitColorScheme") ? localStorage.getItem("dashkitColorScheme") : "light";

//     function a() {
//         Chart.defaults.global.defaultColor = e[700], Chart.defaults.global.defaultFontColor = e[700], Chart.defaults.global.elements.arc.borderColor = e[800], Chart.defaults.global.elements.arc.hoverBorderColor = e[800], Chart.scaleService.updateScaleDefaults("linear", {
//             gridLines: {
//                 borderDash: [2],
//                 borderDashOffset: [2],
//                 color: e[900],
//                 drawBorder: !1,
//                 drawTicks: !1,
//                 zeroLineColor: e[900],
//                 zeroLineBorderDash: [2],
//                 zeroLineBorderDashOffset: [2]
//             },
//             ticks: {
//                 beginAtZero: !0,
//                 padding: 10,
//                 callback: function(e) {
//                     if (!(e % 10)) return e
//                 }
//             }
//         })
//     }a()
   
// }();